//
//  HomeCell.swift
//  JobGet
//
//  Created by Firoz Khursheed on 27/01/22.
//

import Foundation
import UIKit

class HomeCell: UITableViewCell {

  @IBOutlet weak var verticalStackView: UIStackView!

  private var models: [HomeCellModel]?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }

  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
  }

  func set(models: [HomeCellModel]) {
    self.models = models
    verticalStackView.subviews.forEach { (view) in
        view.removeFromSuperview()
    }
    for model in models {
      let rowView = generateLabelRow(item: model)
      verticalStackView?.addArrangedSubview(rowView)
    }

  }

  private func generateLabelRow(item: HomeCellModel) -> UIStackView {
    let stackView = UIStackView()
    stackView.layoutMargins = UIEdgeInsets(top: 2, left: 16, bottom: 2, right: 16)
    stackView.isLayoutMarginsRelativeArrangement = true

    stackView.axis = .horizontal
    stackView.distribution = .fill
    stackView.alignment = .fill
    stackView.spacing = 16.0

    let leftLabel = UILabel()
    leftLabel.text = item.title
    let rightLabel = UILabel()
    rightLabel.text = item.value
    rightLabel.setContentHuggingPriority(.init(rawValue: 800), for: .horizontal)

    stackView.addArrangedSubview(leftLabel)
    stackView.addArrangedSubview(rightLabel)
    return stackView
  }
}

struct HomeCellModel {
  var title: String
  var value: String?
}

