//
//  ViewController.swift
//  JobGet
//
//  Created by Firoz Khursheed on 27/01/22.
//

import UIKit
import DropDown
import CoreData

enum TransactionType: String {
  case expense
  case income
}

class HomeViewController: UIViewController {

  let cellReuseIdentifier = "homecell"
  @IBOutlet weak var tableView: UITableView!

  @IBOutlet weak var expensesLabel: UILabel!
  @IBOutlet weak var incomeLabel: UILabel!
  @IBOutlet weak var balanceLabel: UILabel!
  @IBOutlet weak var expenseMeter: UIProgressView!

  @IBOutlet weak var transactionView: UIView!
  @IBOutlet weak var transactionViewBottomConstraint: NSLayoutConstraint!


  // --- Transaction View
  // TODO: This would be a separate class in actual project
  
  @IBOutlet weak var datePicker: UIDatePicker!
  @IBOutlet weak var transactionDescription: UITextField!
  @IBOutlet weak var amount: UITextField!
  @IBOutlet weak var transactionTypeButton: UIButton!
  let transactionTypeMenu = DropDown()

  private var transactionType = TransactionType.expense
  //

  // --- Fetch results controller
  lazy var fetchedResultsController: NSFetchedResultsController<Transaction> = {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate // Prod code would not have forced type cast
    let managedContext = appDelegate.persistentContainer.viewContext

    let fetchRequest: NSFetchRequest<Transaction>
    fetchRequest = Transaction.fetchRequest()

    let sortDescriptor = NSSortDescriptor(key: "date", ascending: false)
    fetchRequest.sortDescriptors = [sortDescriptor]

    let fetchedResultsController = NSFetchedResultsController<Transaction>(fetchRequest: fetchRequest, managedObjectContext: managedContext, sectionNameKeyPath: "date", cacheName: nil)

    fetchedResultsController.delegate = self

    return fetchedResultsController
  }()
  //

  override func viewDidLoad() {
    super.viewDidLoad()

    transactionTypeMenu.anchorView = transactionTypeButton
    transactionTypeMenu.dataSource = [TransactionType.expense.rawValue, TransactionType.income.rawValue]

    datePicker.datePickerMode = .date
    getTransactions()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    updateFinancialSummary()
    transactionTypeMenu.selectionAction = { [unowned self] (index: Int, title: String) in
      print(index, title)
      switch TransactionType(rawValue: title) {
      case .expense:
        self.transactionType = .expense
        print("Expense")
      case .income:
        self.transactionType = .income
        print("Income")
      default:
        print("Exception, this needs to be handled properly in actual project")
        break;
      }
    }
  }

  @IBAction func newTransaction(_ sender: UIButton) {
    transactionViewBottomConstraint.constant = 0
    animatePopup()
  }

  private func animatePopup() {
    UIView.animate(withDuration: 0.3) {
      self.view.layoutIfNeeded()
    }
  }

  private func updateFinancialSummary() {
    let (expense, income, balanceRemaining) = totalFinance()
    expensesLabel.text = String(expense)
    incomeLabel.text   = String(income)
    balanceLabel.text  = String(balanceRemaining)
    if income != 0 {
      expenseMeter.progress = Float(expense / income)
      expenseMeter.isHidden = false
    } else {
      expenseMeter.isHidden = true
    }
  }

//  private func totalExpense() -> Double {
//    fetchedResultsController.fetchedObjects?.reduce(0.0, { sum, transaction in
//                                                      transaction.type == TransactionType.expense.rawValue ? sum + transaction.amount!.doubleValue : 0}) ?? 0
//  }
//
//  private func totalIncome() -> Double {
//    fetchedResultsController.fetchedObjects?.reduce(0.0, { sum, transaction in
//                                                      transaction.type == TransactionType.income.rawValue ? sum + transaction.amount!.doubleValue : 0}) ?? 0
//  }
//
  private func totalFinance() -> (expense: Double, income: Double, balance: Double) {
    var income = 0.0
    var expense = 0.0
    fetchedResultsController.fetchedObjects?.forEach({ (transaction) in
      if transaction.type == TransactionType.income.rawValue {
        income += transaction.amount?.doubleValue ?? 0
      } else if transaction.type == TransactionType.expense.rawValue {
        expense += transaction.amount?.doubleValue ?? 0
      }
    })

    return (expense, income, income - expense)
  }
}

extension HomeViewController: UITableViewDelegate {}

extension HomeViewController: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    fetchedResultsController.sections?.count ?? 0
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

    let cell: HomeCell =  tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath) as! HomeCell

    cell.layer.masksToBounds = true
    cell.layer.cornerRadius = 4
    cell.layer.borderWidth = 2
    cell.layer.shadowOffset = CGSize(width: -1, height: 1)
    cell.layer.borderColor = UIColor.gray.cgColor

    var models = Array(fetchedResultsController.sections![indexPath.section].objects as [Transaction]).map { (transaction: Transaction) -> HomeCellModel in
      let sign = transaction.type == TransactionType.expense.rawValue ? "-" : ""
      return HomeCellModel(title: transaction.textDescription ?? "", value: "\(sign) $\(transaction.amount!.decimalValue)")
    }

    let date = (fetchedResultsController.sections![indexPath.section].objects?.first as? Transaction)?.date ?? Date()
    let dateFormatter = DateFormatter() // This code would be optimized in Prod
    dateFormatter.dateFormat = "dd MMMM, YYYY"

    models.insert(HomeCellModel(title: dateFormatter.string(from: date)), at: 0)
    cell.set(models: models)
    return cell
  }

  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    24
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    UIView()
  }
}

extension HomeViewController { // Handling Data
  private func getTransactions() {
    do{
      try fetchedResultsController.performFetch()
    }catch{
      print(error)
    }
  }

  private func saveTransaction(date: Date, amount: Double, desc: String, type: TransactionType) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }
    let date = Calendar.current.date(from: Calendar.current.dateComponents([.year, .month, .day], from: date))

    let managedContext = appDelegate.persistentContainer.viewContext

    let transaction             = Transaction(context: managedContext)
    transaction.date            = date
    transaction.amount          = NSDecimalNumber(value: amount)
    transaction.textDescription = desc
    transaction.type            = type.rawValue

    do {
      try managedContext.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
}

extension HomeViewController: NSFetchedResultsControllerDelegate {
  func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
    tableView.reloadData()
    updateFinancialSummary()
  }
}

//-- Transaction Class methods
extension HomeViewController {
  @IBAction func transactionTypeSelection(_ sender: Any) {
    transactionTypeMenu.show()
  }

  @IBAction func addTransaction(_ sender: Any) {
    if let amount = Double(amount.text ?? "0"),
       let desc = transactionDescription.text {
      saveTransaction(date: datePicker.date,
                      amount: amount,
                      desc: desc,
                      type: transactionType)

      transactionViewBottomConstraint.constant = 700; // This would not be hard coded in actual project
      animatePopup()
      resetTransactionData()
    } else {
      let alert = UIAlertController(title: "Error", message: "Missing Data", preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default))
      present(alert, animated: true, completion: nil)
    }
  }

  @IBAction func cancelTransaction(_ sender: Any) {
    transactionViewBottomConstraint.constant = 700; // This would not be hard coded in actual project
    animatePopup()
  }

  private func resetTransactionData() {
    datePicker.datePickerMode = .date
    transactionDescription.text = ""
    amount.text = ""
    transactionType = .expense
  }
}
